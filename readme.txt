Team member  			Email Address
Zijing Liu  			zijingli@usc.edu
Lingxiao Gao   			lingxiag@usc.edu
Zheman Shi  			zhemansh@usc.edu
Thanakorn Suppakarnpanich 	suppakar@usc.edu 
Jie He  			hejie@usc.edu
Yubo Wang 			yubowang@usc.edu


* A text game engine that helps users easily create their own graphical text game without any coding experience.
* Source is under StoryCreator folder. It's a eclipse Java project and could be easily built. Database files are under database folder.